import DefaultLayout from "../layouts/Default.vue";
import AuthLayout from "../layouts/AuthLayout.vue";

export default [
  {
    path: "/",
    component: DefaultLayout,
    children: [
      {
        path: "/",
        name: "home",
        component: () => import("../views/Home.vue"),
      },
    ],
  },

  {
    path: "/auth",
    component: AuthLayout,
    children: [
      {
        path: "login",
        name: "login",
        component: () => import("../views/Login.vue"),
      },
    ],
  },
];
